import 'package:flutter/material.dart';
import 'package:flutter_travel_ui/core/api.dart';
import 'package:flutter_travel_ui/models/buying_house_model.dart';
import 'package:flutter_travel_ui/models/destination_model.dart';
import 'package:flutter_travel_ui/screens/buying_house_detail_screen.dart';
import 'package:flutter_travel_ui/screens/destination_screen.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class ListBuyingHouse extends StatefulWidget {

  ListBuyingHouse({Key key}) : super(key: key);

  @override
  _ListBuyingHouseState createState() =>
      new _ListBuyingHouseState();
}

class _ListBuyingHouseState extends State<ListBuyingHouse>{

  bool _isLoading;
  List<BuyingHouseModel> _list;

  _loadList(List json) {
    print(json);
    try {
      _list = List();
      if (json != null) {
        List<BuyingHouseModel> tmp = List();
        for (int i = 0; i < json.length; i++) {
          tmp.add(BuyingHouseModel.fromJson(json[i]));
        }
        setState(() {
          _list = tmp;
        });
      } else
        Api().showMessage('Error');
    } catch (e) {
      print(e.toString());
    }
  }
  @override
  void initState() {
    super.initState();
    setState(() {
      _isLoading = true;
    });
    Api()
        .connectAPIGetListBuyHouse()
        .then((result) {
          _loadList(result);
      setState(() {
        _isLoading = false;
      });
    });
  }


  @override
  Widget build(BuildContext context) {
    return Container(
        child:  _list!=null && _list.length>0?
        ListView.builder(
          scrollDirection: Axis.vertical,
          itemCount: _list.length,
          itemBuilder: (BuildContext context, int index) {
            BuyingHouseModel buyinghouse = _list[index];
            return GestureDetector(
                onTap: (){
                  print("di chuyen mang hinh");
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => BuyingHouseDetailScreen(buyingHouseModel:buyinghouse,)),
                  );
                },
                child: Container(
                    child: Row(
                      children: <Widget>[
                        new Container(width: 10.0, height: 190.0, color: Colors.white10),
                        new Expanded(
                          child: new Padding(
                            padding:
                            const EdgeInsets.symmetric(vertical: 40.0, horizontal: 20.0),
                            child: new Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                new Text(
                                  buyinghouse.tieudetin,
                                  style: TextStyle(
                                      color: Colors.grey.shade800,
                                      fontSize: 18.0,
                                      fontWeight: FontWeight.bold),
                                ),
                                new Padding(
                                  padding: const EdgeInsets.only(top: 16.0),
                                  child: new Text(
                                    buyinghouse.ngaydangtin,
                                    style: TextStyle(
                                        color: Colors.grey.shade500,
                                        fontSize: 12.0,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        new Container(
                          height: 150.0,
                          width: 150.0,
                          color: Colors.white,
                          child: Stack(
                            children: <Widget>[
                              new Transform.translate(
                                offset: new Offset(50.0, 0.0),
                                child: new Container(
                                  height: 100.0,
                                  width: 100.0,
                                  color: Colors.blue,
                                ),
                              ),
                              new Transform.translate(
                                offset: Offset(10.0, 20.0),
                                child: new Card(
                                  elevation: 20.0,
                                  child: new Container(
                                    height: 120.0,
                                    width: 120.0,
                                    decoration: new BoxDecoration(
                                        color: Colors.white,
                                        border: Border.all(
                                            width: 10.0,
                                            color: Colors.white,
                                            style: BorderStyle.solid),
                                        image: DecorationImage(
                                          image: NetworkImage("https://fidev.io/wp-content/uploads/2019/05/Tickets-advanced-transitions-3.jpg"),
                                        )),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    )
                )
            );
          },
        ): Container()


    );
  }

}

