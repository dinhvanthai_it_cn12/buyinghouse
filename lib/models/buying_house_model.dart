import 'package:flutter_travel_ui/models/activity_model.dart';

import 'activity_model.dart';

class BuyingHouseModel {
  int pkTindangid;
  int fkUserid;
  String ngaydangtin;
  int songaydang;
  int totalview;
  String tieudetin;
  int fkQuanid;
  String street;
  int fkLoaigiaodichid;
  int fkLoaibatdongsanid;
  double gia;
  int dientich;
  int fkTinhtrangphaplyid;
  int sophongngu;
  int  sophongtam;
  int  sotang;
  int  tongsophong;
  String anhmattien;
  String motathem;

  BuyingHouseModel({this.pkTindangid,
    this.fkUserid,
    this.ngaydangtin,
    this.songaydang, this.totalview, this.tieudetin, this.fkQuanid, this.street, this.fkLoaibatdongsanid, this.gia, this.dientich, this.fkTinhtrangphaplyid,
    this.sophongngu, this.sophongtam, this.sotang, this.tongsophong, this.anhmattien, this.motathem
  });

  factory BuyingHouseModel.fromJson(Map json){
    return BuyingHouseModel(
        pkTindangid: json['pkTindangid'],
        fkUserid: json['fkUserid'],
        ngaydangtin:new DateTime.fromMicrosecondsSinceEpoch(json['ngaydangtin']).toString(),
        songaydang: json['songaydang'],
        totalview: json['totalview'],
        tieudetin: json['tieudetin'],
        fkQuanid: json['fkQuanid'],
        street: json['street'],
        fkLoaibatdongsanid: json['fkLoaibatdongsanid'],
        gia: json['gia'],
        dientich: json['dientich'],
        fkTinhtrangphaplyid: json['fkTinhtrangphaplyid'],
        sophongngu: json['sophongngu'],
        sophongtam: json['sophongtam'],
        anhmattien: json['anhmattien'],
        motathem: json['motathem'],
        sotang: json['sotang'],
        tongsophong: json['tongsophong'],
    );
  }
}