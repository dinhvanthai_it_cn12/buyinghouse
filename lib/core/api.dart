import 'dart:io';

import 'package:async/async.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';


const URLBASE = "https://api.myjson.com/bins/iua38";

class Api {
  Future<dynamic> connectAPIGetListBuyHouse() async {
    Map<String, String> headers = Map();
    headers.putIfAbsent('Content-Type', () => 'application/json');

    return http.get('https://api.myjson.com/bins/iua38?fbclid=IwAR0NBVlAxNw7mDjmIoipIJg_ezTVNHx-gYhEMaXgP1i5O7dotsrSh2-8GT8', headers: headers).then((response) {
      if (response != null && response.statusCode == 200) {
        List _result = JsonCodec().decode(utf8.decode(response.bodyBytes));
        return _result;
      }else{
        return null;
      }
    }).catchError((error) {
      showMessage(error.toString());
      return null;
    });

  }

  Future<dynamic> connectEBiddingAPIPost(String controller, String functionName, Object body) {
    Map<String, String> headers = Map();
    headers.putIfAbsent('Content-Type', () => 'application/json');

    return http.post(URLBASE+'$controller/$functionName', body: body).then((response) {
      if (response != null && response.statusCode == 200) {
        //Map<String, dynamic> _result = JsonCodec().decode(utf8.decode(response.bodyBytes));
        Map _result = JsonCodec().decode(utf8.decode(response.bodyBytes));
        if (_result['ResultOK'])
          return _result;
        else
          showMessage(_result['Msg']);
      } else
        showMessage('Response == null or Status Code != 200');
      return null;
    }).catchError((error) {
      showMessage(error.toString());
      return null;
    });
  }

  void showMessage(String msg) {
    //Scaffold.of(scaffoldContext)
    //    .showSnackBar(new SnackBar(content: new Text(msg)));
    Fluttertoast.showToast(msg: msg, toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.BOTTOM, timeInSecForIos: 2, backgroundColor: Colors.blue);
  }

}

//const String domain ="http://10.86.97.248:7896";
//const String domain ="http://10.86.3.3:8088";
const String domain ="https://bidding.laviholding.vn";
const String urlImgUserDefaul= domain + '/Assets/img/Users/avatar-default.png';
const String urlImgUser= domain + '/Assets/img/Users/';
const String urlImgCrop= domain + '/Assets/img/Crops/';
const String urlImgFarm= domain + '/Assets/img/Farms/';
const String urlImgFarmDefault= domain + '/Assets/img/Farms/image-not-available.jpg';
const String urlImgCropDefaul= domain + '/Assets/img/Crops/defauld1.jpg';
