import 'package:flutter/material.dart';
import 'package:flutter_travel_ui/models/activity_model.dart';
import 'package:flutter_travel_ui/models/buying_house_model.dart';
import 'package:flutter_travel_ui/models/destination_model.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class BuyingHouseDetailScreen extends StatefulWidget {
  final BuyingHouseModel buyingHouseModel;

  BuyingHouseDetailScreen({this.buyingHouseModel});

  @override
  _BuyingHouseDetailScreeState createState() => _BuyingHouseDetailScreeState();
}

class _BuyingHouseDetailScreeState extends State<BuyingHouseDetailScreen> {
  Text _buildRatingStars(int rating) {
    String stars = '';
    for (int i = 0; i < rating; i++) {
      stars += '⭐ ';
    }
    stars.trim();
    return Text(stars);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: <Widget>[
          Stack(
            children: <Widget>[
              Container(
                height: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(30.0),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black26,
                      offset: Offset(0.0, 2.0),
                      blurRadius: 6.0,
                    ),
                  ],
                ),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(20.0),
                  child: Image(
                    image: AssetImage("assets/images/saopaulo.jpg"),
                    fit: BoxFit.cover,
                  ),
                )
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 40.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    IconButton(
                      icon: Icon(Icons.arrow_back),
                      iconSize: 30.0,
                      color: Colors.black,
                      onPressed: () => Navigator.pop(context),
                    ),
                    Row(
                      children: <Widget>[
                        IconButton(
                          icon: Icon(Icons.search),
                          iconSize: 30.0,
                          color: Colors.black,
                          onPressed: () => Navigator.pop(context),
                        ),
                        IconButton(
                          icon: Icon(FontAwesomeIcons.sortAmountDown),
                          iconSize: 25.0,
                          color: Colors.black,
                          onPressed: () => Navigator.pop(context),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Positioned(
                left: 20.0,
                bottom: 20.0,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      widget.buyingHouseModel.street,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 30.0,
                        fontWeight: FontWeight.w600,
                        letterSpacing: 1.2,
                      ),
                    ),
                    Row(
                      children: <Widget>[
                        Icon(
                          FontAwesomeIcons.locationArrow,
                          size: 15.0,
                          color: Colors.white70,
                        ),
                        SizedBox(width: 5.0),
                        Text(
                          widget.buyingHouseModel.ngaydangtin,
                          style: TextStyle(
                            color: Colors.white70,
                            fontSize: 20.0,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Positioned(
                right: 20.0,
                bottom: 20.0,
                child: Icon(
                  Icons.location_on,
                  color: Colors.white70,
                  size: 25.0,
                ),
              ),
            ],
          ),

//          Row(
//            children: <Widget>[
//              ListTile(
//                title: new Text("Tiêu đề tin"),
//                subtitle: new Text(
//                  widget.buyingHouseModel.tieudetin,
//                  style: TextStyle(
//                    color: Colors.white,
//                    fontSize: 30.0,
//                    fontWeight: FontWeight.w600,
//                    letterSpacing: 1.2,
//                  ),
//                ),
//              ),
//            ],
//          ),
          /*Expanded(
            child: ListView.builder(
              padding: EdgeInsets.only(top: 10.0, bottom: 15.0),
              itemCount: widget.destination.activities.length,
              itemBuilder: (BuildContext context, int index) {
                Activity activity = widget.destination.activities[index];
                return Stack(
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.fromLTRB(40.0, 5.0, 20.0, 5.0),
                      height: 170.0,
                      width: double.infinity,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                      child: Padding(
                        padding: EdgeInsets.fromLTRB(100.0, 20.0, 20.0, 20.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Container(
                                  width: 120.0,
                                  child: Text(
                                    activity.name,
                                    style: TextStyle(
                                      fontSize: 18.0,
                                      fontWeight: FontWeight.w600,
                                    ),
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 2,
                                  ),
                                ),
                                Column(
                                  children: <Widget>[
                                    Text(
                                      '\$${activity.price}',
                                      style: TextStyle(
                                        fontSize: 22.0,
                                        fontWeight: FontWeight.w600,
                                      ),
                                    ),
                                    Text(
                                      'per pax',
                                      style: TextStyle(
                                        color: Colors.grey,
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                            Text(
                              activity.type,
                              style: TextStyle(
                                color: Colors.grey,
                              ),
                            ),
                            _buildRatingStars(activity.rating),
                            SizedBox(height: 10.0),
                            Row(
                              children: <Widget>[
                                Container(
                                  padding: EdgeInsets.all(5.0),
                                  width: 70.0,
                                  decoration: BoxDecoration(
                                    color: Theme.of(context).accentColor,
                                    borderRadius: BorderRadius.circular(10.0),
                                  ),
                                  alignment: Alignment.center,
                                  child: Text(
                                    activity.startTimes[0],
                                  ),
                                ),
                                SizedBox(width: 10.0),
                                Container(
                                  padding: EdgeInsets.all(5.0),
                                  width: 70.0,
                                  decoration: BoxDecoration(
                                    color: Theme.of(context).accentColor,
                                    borderRadius: BorderRadius.circular(10.0),
                                  ),
                                  alignment: Alignment.center,
                                  child: Text(
                                    activity.startTimes[1],
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                    Positioned(
                      left: 20.0,
                      top: 15.0,
                      bottom: 15.0,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(20.0),
                        child: Image(
                          width: 110.0,
                          image: AssetImage(
                            activity.imageUrl,
                          ),
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ],
                );
              },
            ),
          ),*/
          Divider(),
          new ListTile(
              title: Text("Mã tin đăng: " + widget.buyingHouseModel.pkTindangid.toString(),style: TextStyle(fontSize: 20.0),)
            ,),
          new ListTile(
            title: new Text("Tiêu đề tin", style: TextStyle(fontWeight: FontWeight.bold),),
            subtitle: new Text(widget.buyingHouseModel.tieudetin, style: TextStyle(fontSize: 25.0, fontFamily: "DancingScript",fontWeight: FontWeight.bold,),),
          ),
          new ListTile(
            title: Text("Số ngày đăng: " + widget.buyingHouseModel.songaydang.toString(),style: TextStyle(fontSize: 20.0),)
            ,),
          new ListTile(
            title: Text("Lượt xem: " + widget.buyingHouseModel.totalview.toString(),style: TextStyle(fontSize: 20.0),)
            ,),
          new ListTile(
            title: Text("Số phòng ngủ: " + widget.buyingHouseModel.sophongngu.toString(),style: TextStyle(fontSize: 20.0),)
            ,),
          new ListTile(
            title: Text("Số phong tắm: " + widget.buyingHouseModel.sophongtam.toString(),style: TextStyle(fontSize: 20.0),)
            ,),
          new ListTile(
            title: Text("Tổng số phòng: " + widget.buyingHouseModel.tongsophong.toString(),style: TextStyle(fontSize: 20.0),)
            ,),
          new ListTile(
            title: Text("Số tầng: " + widget.buyingHouseModel.sotang.toString(),style: TextStyle(fontSize: 20.0),)
            ,),
          new ListTile(
            title: Text("Mã user id: " + widget.buyingHouseModel.fkUserid.toString(),style: TextStyle(fontSize: 20.0),)
            ,),
          new ListTile(
            title: new Text("Địa chỉ",style: TextStyle(fontSize: 20.0),),
            subtitle: new Text(widget.buyingHouseModel.street +" "+ widget.buyingHouseModel.fkQuanid.toString(), style: TextStyle(fontSize: 18.0, fontFamily: "DancingScript",),),
          ),
          new ListTile(
          title: new Text("Ghi chú",style: TextStyle(fontSize: 20.0),),
          subtitle: new Text(widget.buyingHouseModel.motathem, style: TextStyle(fontSize: 18.0, fontFamily: "DancingScript",),),
          ),
//          Row(
//            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//            children: <Widget>[
//              Align(
//                child: Container(
//                  child: Column(
//                    children: <Widget>[
//                      Align(
//                        child: Text('Full Name',
//                            style: TextStyle(
//                                color: Colors.blueGrey, fontSize: 18.0)),
//                      ),
//                      Align(
//                        child: Text('Đinh Thị Kim Thoa',
//                            style: TextStyle(
//                                color: Colors.black,
//                                fontSize: 20.0,
//                                fontWeight: FontWeight.bold)),
//                      ),
//                    ],
//                  ),
//                ),
//              ),
//              Align(
//                alignment: Alignment.centerRight,
//                child: Container(
//                  child: Icon(
//                    FontAwesomeIcons.pen,
//                    color: Color(0xff476cfb),
//                  ),
//                ),
//              ),
//            ],
//          ),
          SizedBox(
            height: 20.0,
          ),
        ],
      ),
    );
  }
}