import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_travel_ui/style/AppTheme.dart';
import 'package:flutter_travel_ui/widgets/listbuyinghouse_widget.dart';
import 'package:flutter_travel_ui/widgets/hotel_carousel.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'account_screen.dart';
import 'buying_house_new_screen.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}
class _HomeScreenState extends State<HomeScreen> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Padding(
            padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                appBar(),
                Flexible(
                  child:  ListBuyingHouse(),
                )
              ],
            )
        ),
    );
  }
  Widget appBar() {
    return SizedBox(
      height: AppBar().preferredSize.height,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(top: 8, left: 8),
            child: Container(
              width: AppBar().preferredSize.height - 8,
              height: AppBar().preferredSize.height - 8,
            ),
          ),
          Expanded(
            child: Center(
              child: Padding(
                padding: const EdgeInsets.only(top: 4),
                child: Text(
                    "Danh sách tin đăng",
                    style: AppTheme.title_form
                ),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 8, right: 8),
            child: Container(
              width: AppBar().preferredSize.height - 8,
              height: AppBar().preferredSize.height - 8,
              child: Material(
                color: Colors.transparent,
                child: InkWell(
                  borderRadius:
                  new BorderRadius.circular(AppBar().preferredSize.height),
                  child: Icon(
                    Icons.add,
                    color: AppTheme.dark_grey,
                  ),
                  onTap: () {
                    Navigator.of(context).push(
                      MaterialPageRoute<bool>(
                        builder: (context) => BuyingHouseNewcreen(),
                      ),
                    ).then((value){

                    });
                  },
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
