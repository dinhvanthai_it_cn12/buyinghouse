import 'package:flutter/material.dart';
import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_travel_ui/screens/setting_screen.dart';
import 'package:flutter_travel_ui/widgets/listbuyinghouse_widget.dart';
import 'package:flutter_travel_ui/widgets/hotel_carousel.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'account_screen.dart';
import 'home_screen.dart';

class NavigatorScreen extends StatefulWidget {
  @override
  _NaviState createState() => _NaviState();

}

class _NaviState extends State<NavigatorScreen> {
  int _selectedIndex = 0;

  final _pageOption= [
    HomeScreen(),
    AcountScreen(),
    SettingScreen(),
    SettingScreen(),
  ];

  @override
  void initState() {
    super.initState();

  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _pageOption[_selectedIndex],

      bottomNavigationBar: BottomNavyBar(
      selectedIndex: _selectedIndex,
      showElevation: true,
      itemCornerRadius: 8,
      curve: Curves.easeInBack,
      onItemSelected: (index) => setState(() {
        _selectedIndex = index;
      }),
      items: [
        BottomNavyBarItem(
          icon: Icon(Icons.apps),
          title: Text('Home'),
          activeColor: Colors.red,
          textAlign: TextAlign.center,
        ),
        BottomNavyBarItem(
          icon: Icon(Icons.people),
          title: Text('Users'),
          activeColor: Colors.purpleAccent,
          textAlign: TextAlign.center,
        ),
        BottomNavyBarItem(
          icon: Icon(Icons.message),
          title: Text(
            'Messages test for mes teset test test ',
          ),
          activeColor: Colors.pink,
          textAlign: TextAlign.center,
        ),
        BottomNavyBarItem(
          icon: Icon(Icons.settings),
          title: Text('Settings'),
          activeColor: Colors.blue,
          textAlign: TextAlign.center,
        ),
      ],
    ),
    );
  }
}
