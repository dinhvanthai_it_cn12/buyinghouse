import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_travel_ui/style/AppTheme.dart';
import 'package:flutter_travel_ui/widgets/listbuyinghouse_widget.dart';
import 'package:flutter_travel_ui/widgets/hotel_carousel.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'account_screen.dart';

class BuyingHouseNewcreen  extends StatefulWidget {
  @override
  _BuyingHouseNewcreenState createState() => _BuyingHouseNewcreenState();
}
class _BuyingHouseNewcreenState extends State<BuyingHouseNewcreen> {
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0, color: Colors.black);
  TextEditingController phone = new TextEditingController();
  TextEditingController pass = new  TextEditingController();

  TextEditingController tendangnhap = new TextEditingController();
  TextEditingController hoten = new  TextEditingController();
  TextEditingController socmnd = new  TextEditingController();
  TextEditingController email = new  TextEditingController();
  TextEditingController passwordconfirm = new  TextEditingController();

  Widget tendangnhapwidget(){
    return TextField(
      controller: tendangnhap,
      style: style,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          labelText: "Tên đăng nhập",
          border:
          OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
    );
  }
  Widget sodienthoaiwidget(){
    return TextField(
      controller: phone,
      style: style,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          labelText: "Số điện thoại",
          border:
          OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
    );
  }
  Widget hotenwidget(){
    return TextField(
      controller: hoten,
      style: style,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          labelText: "Họ tên",
          border:
          OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
    );
  }
  Widget emailwidget(){
    return TextField(
      controller: email,
      style: style,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          labelText: "Email",
          border:
          OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
    );
  }
  Widget socmndwidget(){
    return TextField(
      controller: socmnd,
      style: style,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          labelText: "Số CMND",
          border:
          OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
    );
  }
  Widget passwidget(){
    return TextField(
      controller: pass,
      obscureText: true,
      style: style,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          labelText: "Mật khẩu",
          border:
          OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
    );
  }
  Widget passConfirmwidget(){
    return TextField(
      controller: passwordconfirm,
      style: style,
      obscureText: true,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          labelText: "Nhập lại MK",
          border:
          OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Padding(
            padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                appBar(),
                SingleChildScrollView(
                  child: Padding(
                      padding: const EdgeInsets.all(36.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          SizedBox(
                            height: 155.0,
                            child: Image.asset(
                              "assets/images/icon_app_efarm_tranparent.png",
                              fit: BoxFit.contain,
                            ),
                          ),
                          SizedBox(height: 15.0),
                          tendangnhapwidget(),
                          SizedBox(height: 15.0),
                          hotenwidget(),
                          SizedBox(height: 15.0),
                          sodienthoaiwidget(),
                          SizedBox(height: 15.0),
                          emailwidget(),
                          SizedBox(height: 15.0),
                          socmndwidget(),
                          SizedBox(height: 15.0),
                          passwidget(),
                          SizedBox(
                            height: 15.0,
                          ),
                          //passConfirm(),
                          //SizedBox(height: 25.0),

                        ],
                      )
                  ),

                ),
              ],
            )
        ),
    );
  }
  Widget appBar() {
    return SizedBox(
      height: AppBar().preferredSize.height,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(top: 8, left: 8),
            child: Container(
              width: AppBar().preferredSize.height - 8,
              height: AppBar().preferredSize.height - 8,
            ),
          ),
          Expanded(
            child: Center(
              child: Padding(
                padding: const EdgeInsets.only(top: 4),
                child: Text(
                    "Thêm mới tin đăng",
                    style: AppTheme.title_form
                ),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 8, right: 8),
            child: Container(
              width: AppBar().preferredSize.height - 8,
              height: AppBar().preferredSize.height - 8,
              child: Container()
            ),
          ),
        ],
      ),
    );
  }
}
