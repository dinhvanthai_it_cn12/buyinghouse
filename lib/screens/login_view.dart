import 'package:flutter/material.dart';
import 'dart:convert' ;
import 'package:http/http.dart' as http;
import 'package:flutter/cupertino.dart';

import 'home_screen.dart';
import 'navigator_screen.dart';
class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);

  TextEditingController emailControler = new TextEditingController();
  TextEditingController passControler = new TextEditingController();
  bool _isloading =false;
  @override
  Widget build(BuildContext context) {

    final emailField = TextField(
      style: style,
      controller: emailControler,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: "Email",
          border:
          OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
    );

    final passwordField = TextField(
      obscureText: true,
      style: style,
      controller: passControler,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: "Password",
          border:
          OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
    );

    final loginButon = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: Color(0xff01A0C7),
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        onPressed: () {
          print(emailControler.text);
          print(passControler.text);
          _checkvaluelogin(emailControler.text, passControler.text);
        },
        child: !_isloading? Text("Login",
            textAlign: TextAlign.center,
            style: style.copyWith(
                color: Colors.white, fontWeight: FontWeight.bold)) : CupertinoActivityIndicator(),
      ),
    );

    return Scaffold(
        body: SingleChildScrollView(
          child: Center(
            child: Container(
              color: Colors.white,
              child: Padding(
                padding: const EdgeInsets.all(36.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                      height: 155.0,
                      child: Image.asset(
                        "assets/logo.png",
                        fit: BoxFit.contain,
                      ),
                    ),
                    SizedBox(height: 45.0),
                    emailField,
                    SizedBox(height: 25.0),
                    passwordField,
                    SizedBox(
                      height: 35.0,
                    ),
                    loginButon,
                    SizedBox(
                      height: 15.0,
                    ),
                  ],
                ),
              ),
            ),
          ),
        )
    );
  }

  _checkvaluelogin(String email, String pass) async{

    if(email.isNotEmpty && pass.isNotEmpty && email == "1" && pass =="1"){
      setState(() {
        _isloading=true;
      });
      Future.delayed(const Duration(milliseconds: 2000), () {
        setState(() {
          _isloading=false;
        });
        Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => NavigatorScreen()));

      });
      // xử lý gửi email và pass lên cho services xử lý
      //trước khi gửi lên thì cho hiển thị xoay loadding, rồi sau khi trả được kết quả về thì tắt cái loading đi
      print("send");

      //http.Response response = await http.get("https://api.myjson.com/bins/xythg"
      //);
      //data = json.decode(response.body);
      //Map data = json.decode(utf8.decode(response.bodyBytes));
      //print(data);
      //if(data!=null){// dã sử trường hợp đúng


      //}else{

     // }

      //debugPrint(response.toString());
    }else{
      //thông báo yêu cầu nhập user nam và pass
      print("Email hoạc pass đã nhập sai.");

    }
  }
}